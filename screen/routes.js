import React from 'react';
import Login from './Login';
import Register from './Register';
import { createAppContainer, createSwitchNavigator} from 'react-navigation';

const Routes = createAppContainer(createSwitchNavigator({
    Login,
    Register,
    }
));

export default Routes;