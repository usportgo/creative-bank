import React, {useState, useEffect} from 'react';
import { 
  Container, 
  Inputs, 
  ButtonOpacitty, 
  ButtonText, 
  InvisbleButton,
  TextRegister, 
  BoxHeader, 
  IconHeader, 
  TextHeader, 
  IconLogin, 
  IconPassword, 
  InputBox, 
} from './styles';

export default function Login({ navigation }) {
  return (
    <Container>
        <BoxHeader>
          <IconHeader name="user-circle-o" size={150} color="#2F90FF"/>
          <TextHeader>SEJA BEM-VINDO!</TextHeader>
        </BoxHeader>
        <InputBox>
          <Inputs 
            placeholder={"Digite seu email"}
            placeholderTextColor="#fff" 
            underlineColorAndroid="transparent">
          </Inputs>
        </InputBox>
        <InputBox>
          <Inputs 
            placeholder={"Digite sua senha"}
            placeholderTextColor="#fff" 
            underlineColorAndroid="transparent"
            secureTextEntry={true}
          >
        </Inputs>
        </InputBox>
        <ButtonOpacitty>
            <ButtonText>ENTRAR</ButtonText>
        </ButtonOpacitty>
        <InvisbleButton onPress={() => navigation.navigate('Register')}>
          <TextRegister>OU CRIAR UMA CONTA</TextRegister>
        </InvisbleButton>
    </Container>
  );
}
