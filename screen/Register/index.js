import React from 'react';
import { 
  Container, 
  InputBox, 
  Inputs, 
  TextInput,  
  BoxHeader, 
  IconHeader,
  TextHeader,
  MiniTextHeader,
  ButtonOpacitty} from './styles';
import { ButtonText } from '../Login/styles';

export default function Register({ navigation }) {
  return (
    <Container>
      <BoxHeader>
        <IconHeader name='user-plus' size={120} color="#2F90FF"/>
        <TextHeader>CRIAR UMA CONTA</TextHeader>
        <MiniTextHeader>Crie uma conta para aproveitar maiores benefícios como criar seu próprio formulário e muito mais.</MiniTextHeader>
      </BoxHeader>
      <InputBox>
          <TextInput>EMAIL</TextInput>
        <Inputs/>
      </InputBox>
      <InputBox>
          <TextInput>SENHA</TextInput>
        <Inputs/>
      </InputBox>
      <InputBox>
          <TextInput>CONFIRMAR</TextInput>
        <Inputs/>
      </InputBox>
      <ButtonOpacitty onPress={() => navigation.navigate('Login')}>
        <ButtonText>CRIAR</ButtonText>
      </ButtonOpacitty>
    </Container>
  );
}
