import styled from 'styled-components/native';
import { FontAwesome } from '@expo/vector-icons';

export const Container = styled.View`
  flex: 1;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

export const BoxHeader = styled.SafeAreaView`
    justify-content: center;
    align-items: center;
    padding-bottom: 80px;
`;

export const IconHeader = styled(FontAwesome)`
    padding-bottom: 20px;
`;

export const TextHeader = styled.Text`
    font-size: 24px;
    color: #2F90FF;
    font-weight: bold;
    margin-bottom: 15px;
`;

export const MiniTextHeader = styled.Text`
    font-size: 16px;
    color: #2F90FF;
    padding-bottom: 5px;
    padding-left: 20px;
    padding-right: 20px;
`;

export const InputBox = styled.SafeAreaView`
    align-items: center;
    justify-content: center;
    margin-bottom: 15px;
    width: 100%;
`;

export const Inputs = styled.TextInput`
    background-color: #2F90FF;
    width: 80%;
    height: 40px;
    padding-left: 30px;
    border: solid 1px;
    border-color: black;
    border-radius: 40px;
    color: white;
`;

export const TextInput = styled.Text`
    font-size: 16px;
    color: #2F90FF;
    padding-bottom: 5px;
`;

export const ButtonOpacitty = styled.TouchableOpacity`
    background-color: #2F90FF;
    width: 37%;
    height: 40px;
    border: solid 1px;
    border-color: black;
    border-radius: 40px;
    justify-content: center;
    align-items: center;
    margin-bottom: 4px;
`;

export const ButtonText = styled.Text`
    font-size: 18px;
    color: white;
`;
