import React from 'react';
import Routes from './screen/routes';

// import {View, Text} from 'react-native';
// import { AppLoading } from 'expo';
// import * as Font from 'expo-font';
// import { Ionicons } from '@expo/vector-icons';

export default function App() {
  // const [state, setState] = useState(false);
  // useEffect(() => {
  //   async function loadingFonts() {
  //     await Font.loadAsync({
  //       Roboto: require('native-base/Fonts/Roboto.ttf'),
  //       Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
  //       ...Ionicons.font,
  //     });
  //     setState(true);
  //   }
  // }, [state]);

  // if (state === false){
  //   return <AppLoading/>;
  // }

  return (
    <Routes/>
  );
}
