import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Main from './page/Main';
import Title from './page/Title';

const AppNavigator = createStackNavigator(
  {
    Main,
    Title
  },
);

export default createAppContainer(
  AppNavigator
);
