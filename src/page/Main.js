import React, { Component } from 'react';
import api from '../services/api';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

export default class Main extends Component {
  static navigationOptions = {
    title: "Pagina Main",
    headerStyle:{
      backgroundColor: "#DA552F"
    },
    headerTintColor: "#FFF"
  };

  state = {
    counter: 0,
    dataInfo: [],
    movies: [],
    page: 1,
  };

  componentDidMount() {
    this.loadTitle();
  }

  loadTitle = async (page = 1) => {
    const response = await api.get('react-native/movies.json');

    const { movies, ...dataInfo } = response.data;
    let items = movies;

    if (page > 1) {
      const newList = items.map(item => {
        return Object.assign({}, item, {
          id: ((page * parseInt(item.id))*5).toString()
        });
      });

      items = this.state.movies.concat(newList);
    }

    this.setState({
      counter: movies.length,
      movies: items,
      dataInfo: Object.assign({}, dataInfo, {
        page: 2,
      }),
      page
    });
  }

  renderItem = ({ item }) => (
    <View style={styles.containerTitle}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          this.props.navigation.navigate('Title', { title: item })
        }}>
        <Text style={styles.title}>{item.title}</Text>
        <Text style={styles.releaseYear}>{item.releaseYear}</Text>
      </TouchableOpacity>
    </View>
  )

  loadMore = () => {

    const {
      page,
      dataInfo,
    } = this.state;

    if (page === dataInfo.page) return;

    const pageNumber = page + 1;
    this.loadTitle(pageNumber);
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          contentContainerStyle={styles.list}
          data={this.state.movies}
          keyExtractor={item => item.id}
          renderItem={this.renderItem}
          onEndReached={this.loadMore}
          onEndReachedThreshold={0.1} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAFAFA',
  },
  list: {
    padding: 20,
  },
  containerTitle: {
    backgroundColor: '#FFFFFF',
    borderWidth: 1,
    borderColor: '#DDDDDD',
    borderRadius: 5,
    padding: 20,
    marginBottom: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333333',
  },
  releaseYear: {
    fontSize: 16,
    color: '#999999',
    marginTop: 5,
    lineHeight: 24,
  },
  button: {
    height: 65,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
});
