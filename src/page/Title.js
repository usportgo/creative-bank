import React from 'react';
import { Text } from 'react-native'

const Title = () => <Text>!!! Form Aqui !!!</Text>;

Title.navigationOptions = ({ navigation }) => ({
  title: navigation.state.params.title.title,
  headerStyle: {
    backgroundColor: "#DA552F"
  },
  headerTintColor: "#FFF"
});

export default Title;
